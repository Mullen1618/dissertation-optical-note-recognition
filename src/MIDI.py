import pygame
from midiutil.MidiFile import MIDIFile


def play_music(file):
    """
    Plays a midi file
    Taken from the internet
    Parameters:
        file - The midi file to be played
    """
    clock = pygame.time.Clock()
    try:
        pygame.mixer.music.load(file)
    except pygame.error:
        raise
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        clock.tick(30)


def stop_music():
    """
    Stops any music played by the mixer
    """
    pygame.mixer.music.stop()


def create_MIDI(file_name):
    """
    Create an empty midi file from midiutil ready to be filled with data
    Parameters
        file_name - the name of the midi file
    """
    midi = MIDIFile(1)
    track = 0
    time = 0
    midi.addTrackName(track, time, file_name)
    midi.addTempo(track, time, 120)
    return midi


def convert_to_MIDI(midi, notes, sharp_notes, flat_notes, file_name):
    """
    Adds a list of notes and rests into a midi file
    Parameters
        midi - The midiutil object that the notes are added to
        notes - The list of notes and rests
        sharp_notes - The sharp notes in the keys
        flat_notes - The flat ntoes in the keys
        file_name - The name of the midi file
    """
    track = 0
    channel = 0
    time = 0
    duration = 1
    volume = 100
    i = 0

    while i < len(notes):
        if notes[i].pitch in sharp_notes:
            notes[i].pitch += 1

        if notes[i].pitch in flat_notes:
            notes[i].pitch -= 1

        if notes[i].rest is 1:
            time += notes[i].length
        else:
            midi.addNote(track, channel, notes[i].pitch, time, notes[i].length, volume)
            if i + 1 < len(notes) and abs(notes[i].x - notes[i + 1].x) > 30:
                time += notes[i].length
        i += 1

    bin_file = open(file_name + ".mid", 'wb')  # TODO check if file can be opened
    midi.writeFile(bin_file)
    bin_file.close()
    return 1


# Initialise values needed for MIDI playback
freq = 44100
bit_size = -16
channels = 2
buffer = 1024
pygame.mixer.init(freq, bit_size, channels, buffer)
pygame.mixer.music.set_volume(0.8)
