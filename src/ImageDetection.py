import cv2, sys, os, MIDI, math
import numpy as np
from PIL import Image, ImageChops, ImageTk


class Note:
    pitch = 0
    rest = 0

    def __init__(self, x, y, length):
        self.x = x
        self.y = y
        self.length = length


def convert(file, message):
    """
    Attempts to convert a file into a MIDI file
    Parameters:
        file - The file to be converted
        message - The GUI element used to send messages to the user
    """
    global stave_distance
    global HEIGHT, WIDTH
    if os.path.isfile(file):
        file_name = (file[:file.rfind('.')])  # Get the file everything from the file apart from the extension
        file_extension = (file[file.rfind('.'):])  # Get the extension of the file
        if file_extension == ".png" or file_extension == ".jpg" or file_extension == ".tif":
            img = load_image(file_name + file_extension)
            HEIGHT, WIDTH = img.shape
            clef = detect_clef(img)
            if clef == -1:
                message.set("FAILED: Could not find a clef")
                return 0

            try:
                lines = find_lines(img)  # Get boundaries of each stave
            except IndexError:
                message.set("FAILED: Image is not suitable for conversion")
                return 0
            except TypeError:
                message.set("FAILED: Image is not suitable for conversion")
                return 0

            notes = []
            for i, line in enumerate(lines):
                temp = img[line[0]:line[1], 0:WIDTH]
                if i == 0:
                    tempNotes, key_sharps, key_flats = run_line(temp, clef, stave_distance,
                                                                i)  # Store the key along with the notes if on first line
                else:
                    tempNotes, _, _ = run_line(temp, clef, stave_distance,
                                               i)  # Just store notes if on any other line
                notes += tempNotes  # Add the notes from different lines together

            key_sharps = add_octaves(key_sharps)
            key_flats = add_octaves(key_flats)

            midi = MIDI.create_MIDI(file_name)
            if MIDI.convert_to_MIDI(midi, notes, key_sharps, key_flats, file_name):
                message.set("Success! Do you want to play the file?")
                return 1
            else:
                message.set("FAILED: Something went wrong")
                return 0

        else:
            message.set("FAILED: File must be an image")
    else:
        message.set("FAILED: File does not exist")


def load_image(file_name):
    """
    Loads an image
    Trims white-space from the border of the image
    Converts image to grey-scale, inverts it and returns it
    Parameters:
        file_name - The image to be loaded
    """
    img = Image.open(file_name)
    img = trim_border(img)
    img = np.array(img)  # Convert image to format used by openCV
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert to greyscale
    ret, img = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)  # Invert colours
    return img


def load_image_for_GUI(file_name):
    """
    Loads an image
    Trims white-space from the border of the image
    Scales the image to fit on the GUI
    Returns the image converted to a form displayable by tkinter
    Parameters:
        file_name - The image to be loaded
    """
    try:
        img = Image.open(file_name)
        img = trim_border(img)
        size = 600, 600
        img.thumbnail(size, Image.ANTIALIAS)  # Scale image to fit GUI
        return ImageTk.PhotoImage(img)
    except IOError:
        return 0


def trim_border(img):
    """
    Trims any whitespace from the edges of an image
    This code was taken from the user fraxel on StackOverflow
    Parameters:
        img - The image to be cropped
    """
    bg = Image.new(img.mode, img.size, img.getpixel((0, 0)))
    diff = ImageChops.difference(img, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    return img.crop(bbox)


def run_line(img, clef, stave_distance, index):
    """
    Converts a single line of sheet music into a list of notes and rests
    Parameters:
        img - The image of the line of sheet music
        clef - Either "Treble" or "Bass"
        stave_distance - The distance between each stave line in pixels
        index - The index of the line being converted
    """
    notes = []
    img = remove_stave_lines(img)
    img = remove_stems_and_bars(img)
    notes = detect_all_notes(img, notes, clef)
    if index == 0:
        key_sharps = detect_sharps(img, notes, clef, stave_distance)
        key_flats = detect_flats(img, notes, clef, stave_distance)
    else:
        detect_sharps(img, notes, clef, stave_distance)
        detect_flats(img, notes, clef, stave_distance)
        key_sharps = []
        key_flats = []
    detect_rests(img, notes)
    return notes, key_sharps, key_flats


def find_lines(img):
    """
    Finds all stave lines in an image
    Groups the lines into sets of 5
    Returns a list of the top postion and bottom position of each stave
    Parameters:
        img - The image of the entire sheet music
    """
    global WIDTH, HEIGHT, stave_distance
    try:
        stave_lines = []
        lines = cv2.HoughLines(img, 1, np.pi / 180, 1500)

        for line in lines:
            for rho, theta in line:
                y = np.sin(theta) * rho
                stave_lines.append(y)

        stave_lines.sort()  # Sort the list based on the x positions
        stave_lines = [x for x in stave_lines if
                       x % 1 == 0]  # Had a bug where there was a real number in the list. This eliminates real numbers
        stave_distance = stave_lines[1] - stave_lines[0]
        i = 0
        no = 5  # Number of notes above or below stave lines to allow
        line_pos = []
        while i < len(stave_lines):
            top = stave_lines[i] - (no * stave_distance)  # The top y coordinate of the stave
            bottom = stave_lines[i + 4] + (no * stave_distance)  # The bottom y coordinate of the stave
            line_pos.append((top if top > 0 else 0,
                             bottom if bottom < HEIGHT else HEIGHT))  # Check if top and bottom dont go outside the bounds of the image
            i += 5
        return line_pos
    except IndexError:
        raise
    except TypeError:
        raise


def remove_stave_lines(img):
    """
    Removes all stave lines from the image
    Parameters:
        img - An image of a single line  of sheet music
    """
    global stave, WIDTH
    stave = []
    while True:
        lines = cv2.HoughLines(img, 1, np.pi / 180, 1500)  # Finds any lines of length 1500
        if lines is None:
            break
        for line in lines:
            for rho, theta in line:
                y = np.sin(theta) * rho
                if len(stave) < 5:
                    stave.append(y)  # Add the first five lines to the stave
                for x in range(0, WIDTH):
                    if img[y - 1, x] == 0:
                        img[y, x] = 0  # Replace the line with black pixels

    stave.sort()
    return img


def remove_stems_and_bars(img):
    """
    Removes all note stems and bar lines from the image
    Parameters:
        img - An image of a single line of sheet music
    """
    height, _ = img.shape
    condition = True
    while condition:
        condition = False
        lines = cv2.HoughLines(img, 1, np.pi / 180, 60)  # Finds any lines of length 60
        if lines is None:
            break
        for line in lines:
            for rho, theta in line:
                if theta == 0.0:  # If line is vertical
                    condition = True
                    x = np.cos(theta) * rho
                    for y in range(0, height):
                        if img[y, x - 1] == 0:
                            img[y, x] = 0  # Replace the line with black pixels

    return img


def detect_sharps(img, notes, clef, distance):
    """
    Detect all sharps in a line of music
    If they are at the start of the line they are part of the key
    If they are next to a note then that notes pitch is increased by 1
    Parameters:
        img - An image of a single line of sheet music
        notes - The list of notes in a line
        clef - Either "Treble" or "Bass"
        distance - The distance between the stave lines
    """
    sharp_notes = []
    height, _ = load_image("template/sharp.png").shape
    for x in detect_note(img, "template/sharp.png", note_length=None, threshold=0.8):
        sharp_notes.append((x.x, x.y))

    sharp_index = 0
    note_index = 0
    key_sharps = []
    while sharp_index < len(sharp_notes) and note_index < len(
            notes):  # Iterates through sharps and notes assigning sharps to a note or to the key
        if notes[0].x - sharp_notes[sharp_index][0] > 20:
            key_sharps.append(
                notes_position_to_pitch(sharp_notes[sharp_index][1] + height / 4, distance, distance / 2, clef))
            sharp_index += 1
        elif 0 < notes[note_index].x - sharp_notes[sharp_index][0] < 100 and abs(
                        notes[note_index].y - sharp_notes[sharp_index][1]) <= 11:
            notes[note_index].pitch += 1
            sharp_index += 1
            note_index = [note.x for note in notes].index(notes[
                                                              note_index].x)  # Go back to first instance with the current note x position. This is for detecting sharps in chords
        else:
            note_index += 1

    return key_sharps


def detect_flats(img, notes, clef, distance):
    """
    Detect all flats in a line of music
    If they are at the start of the line they are part of the key
    If they are next to a note then that notes pitch is decreased by 1
    Parameters:
        img - An image of a single line of sheet music
        notes - The list of notes in a line
        clef - Either "Treble" or "Bass"
        distance - The distance between the stave lines
    """
    flat_notes = []
    height, _ = load_image("template/flat.png").shape
    for x in detect_note(img, "template/flat.png", note_length=None):
        flat_notes.append((x.x, x.y))
    flat_index = 0
    note_index = 0
    key_flats = []
    while flat_index < len(flat_notes) and note_index < len(
            notes):  # Iterates through flats and notes assigning flats to a note or to the key
        if notes[0].x - flat_notes[flat_index][0] > 20:
            key_flats.append(
                notes_position_to_pitch(flat_notes[flat_index][1] + height / 2, distance, distance / 2, clef))
            flat_index += 1
        elif 0 < notes[note_index].x - flat_notes[flat_index][0] < 100 and 15 < notes[note_index].y - \
                flat_notes[flat_index][
                    1] < 35:
            notes[note_index].pitch -= 1
            flat_index += 1
            note_index = [note.x for note in notes].index(notes[
                                                              note_index].x)  # Go back to first instance with the current note x position. This is for detecting sharps in chords
        else:
            note_index += 1

    return key_flats


def add_octaves(key):
    """
    Add two octaves up and down to the key
    Parameters:
        key - The list of notes in the key
    """
    temp = []
    for x in key:
        temp.append(x - 12)  # 12 semitones in an octave
        temp.append(x - 24)
        temp.append(x + 12)
        temp.append(x + 24)

    key += temp
    return key


def notes_position_to_pitch(y, distance, half_distance, clef):
    """
    Converts a y coordinate into a pitch
    Takes into account the clef
    Parameters
        y - The y position of a note
        distance - the distance between two stave lines
        half_distance - half the distance between two stave lines
        clef - Either "Treble" or "Bass"
    """
    global stave
    if clef is "Treble":
        if stave[0] - (distance * 3) - half_distance <= y < stave[0] - (distance * 3):
            return 88
        elif stave[0] - (distance * 3) <= y < stave[0] - (distance * 2) - half_distance:
            return 86
        elif stave[0] - (distance * 2) - half_distance <= y < stave[0] - (distance * 2):
            return 84
        elif stave[0] - (distance * 2) <= y < stave[0] - distance - half_distance:
            return 83
        elif stave[0] - distance - half_distance <= y < stave[0] - distance:
            return 81
        elif stave[0] - distance <= y < stave[0] - half_distance:
            return 79
        if stave[0] - half_distance <= y < stave[1] - distance:
            return 77
        elif stave[0] <= y < stave[1] - half_distance:
            return 76
        elif stave[0] + half_distance <= y < stave[1]:
            return 74
        elif stave[1] <= y < stave[2] - half_distance:
            return 72
        elif stave[1] + half_distance <= y < stave[2]:
            return 71
        elif stave[2] <= y < stave[3] - half_distance:
            return 69
        elif stave[2] + half_distance <= y < stave[3]:
            return 67
        elif stave[3] <= y < stave[4] - half_distance:
            return 65
        elif stave[3] + half_distance <= y < stave[4]:
            return 64
        elif stave[4] <= y < stave[4] + half_distance:
            return 62
        elif stave[4] + half_distance <= y < stave[4] + distance:
            return 60
        elif stave[4] + distance <= y < stave[4] + distance + half_distance:
            return 59
        elif stave[4] + half_distance + distance <= y < stave[4] + 2 * distance:
            return 57
        elif stave[4] + 2 * distance <= y < stave[4] + 2 * distance + half_distance:
            return 55
        elif stave[4] + 2 * distance + half_distance <= y < stave[4] + 3 * distance:
            return 53
        elif stave[4] + 3 * distance <= y < stave[4] + 3 * distance + half_distance:
            return 52
        else:
            return 0
    elif clef is "Bass":
        if stave[0] - (distance * 3) - half_distance <= y < stave[0] - (distance * 3):
            return 91
        elif stave[0] - (distance * 3) <= y < stave[0] - (distance * 2) - half_distance:
            return 89
        elif stave[0] - (distance * 2) - half_distance <= y < stave[0] - (distance * 2):
            return 88
        elif stave[0] - (distance * 2) <= y < stave[0] - distance - half_distance:
            return 86
        elif stave[0] - distance - half_distance <= y < stave[0] - distance:
            return 84
        elif stave[0] - distance <= y < stave[0] - half_distance:
            return 83
        if stave[0] - half_distance <= y < stave[1] - distance:
            return 81
        elif stave[0] <= y < stave[1] - half_distance:
            return 79
        elif stave[0] + half_distance <= y < stave[1]:
            return 77
        elif stave[1] <= y < stave[2] - half_distance:
            return 76
        elif stave[1] + half_distance <= y < stave[2]:
            return 74
        elif stave[2] <= y < stave[3] - half_distance:
            return 72
        elif stave[2] + half_distance <= y < stave[3]:
            return 71
        elif stave[3] <= y < stave[4] - half_distance:
            return 69
        elif stave[3] + half_distance <= y < stave[4]:
            return 67
        elif stave[4] <= y < stave[4] + half_distance:
            return 65
        elif stave[4] + half_distance <= y < stave[4] + distance:
            return 64
        elif stave[4] + distance <= y < stave[4] + distance + half_distance:
            return 62
        elif stave[4] + half_distance + distance <= y < stave[4] + 2 * distance:
            return 60
        elif stave[4] + 2 * distance <= y < stave[4] + 2 * distance + half_distance:
            return 59
        elif stave[4] + 2 * distance + half_distance <= y < stave[4] + 3 * distance:
            return 57
        elif stave[4] + 3 * distance <= y < stave[4] + 3 * distance + half_distance:
            return 55
        else:
            return 0
    else:
        return 0


def detect_rests(img, notes):
    """
    Detect all the rests in a line of sheet music
    Adds all the rests to the list of notes
    Parameters
        img - An image of a line of sheet music
        notes - The list of notes on this line
    """
    global stave
    rests = []
    last_x = 0
    for x in detect_note(img, "template/block.png", 2, 0.95):
        if (x.x - last_x) > 15:
            if abs(x.y - stave[1]) > abs(x.y - stave[2]):  # If the rest is just above a stave line then it has length 2
                x.length = 2
            else:
                x.length = 4
            rests.append(x)
        last_x = x.x

    rests += detect_note(img, "template/quaverRest.png", 0.5)
    rests += detect_note(img, "template/crotchetRest.png", 1)

    for x in rests:
        x.rest = 1  # Set to being a rest

    notes += rests
    notes.sort(key=lambda x: x.x)  # Sort the list based on the x positions


def detect_clef(img):
    """
    Detect the clef of the sheet music
    Parameters
        img - An image of an entire piece of sheet music
    """
    if len(template_match(img, load_image("template/trebleClef.png"), 0.8)) > 0:
        return "Treble"
    elif len(template_match(img, load_image("template/bassClef.png"), 0.8)) > 0:
        return "Bass"
    else:
        return -1


def detect_all_notes(img, notes, clef):
    """
        Detect all the notes in a line of sheet music
        Parameters
            img - An image of a line of sheet music
            notes - A list of notes on the line of sheet music
            clef - Either "Treble" or "Bass"
    """
    global stave

    notes += detect_note(img, "template/crotchetHead.png", 1, 0.87)
    detect_quavers(img, notes)

    notes += detect_note(img, "template/minimHead.png", 2)
    notes += detect_note(img, "template/semibreve.png", 4)

    notes.sort(key=lambda x: x.x)  # Sort the list based on the x positions

    distance = stave[1] - stave[0]
    for note in notes:
        note.pitch = notes_position_to_pitch(note.y, distance, int(distance / 2),
                                             clef)  # Assign pitches to all of the notes base on their y position

    return notes


def detect_quavers(img, notes):
    """
        Detect all the quaver tails in a line of sheet music
        Change the length of the nearest crotchet to each tail to be 0.5
        Parameters
            img - An image of a line of sheet music
            notes - A list of notes on the line of sheet music
    """
    quavers = detect_note(img, "template/quaverDown.png", 0.8) + detect_note(img, "template/quaverUp.png", 0.8)
    quavers.sort(key=lambda x: x.x)
    quaver_index = 0
    crotchet_index = 0

    while quaver_index < len(quavers) and crotchet_index < len(
            notes):  # Iterates through quaver tails and crotchets changing the length of the nearest note to each tail
        if quavers[quaver_index].x - notes[crotchet_index].x < 50:
            notes[crotchet_index].length = 0.5
            if crotchet_index + 1 < len(notes) and notes[crotchet_index + 1].x - notes[crotchet_index].x > 10:
                quaver_index += 1
            crotchet_index += 1
        else:
            crotchet_index += 1


def detect_note(img, template_name, note_length=None, threshold=0.8):
    """
    Detect a single type of note in a line of sheet music
    Parameters
        img - An image of a line of sheet music
        template_name - the path of the template image of the note to detectAllNotes
        threshold - the threshold for detecting the template
    """
    new_notes = []
    for pt in template_match(img, load_image(template_name), threshold):
        new_notes.append(Note(pt[0], pt[1] + 5, note_length))

    return new_notes


def template_match(img, template, threshold):
    """
        Find the position of a template image in an image above a certain threshold
        Removes duplicates if they are too close
        Parameters
            img - The image to search for the template in
            template - The template image
            threshold - A value to determine whether the template image is in the image
    """

    method = eval('cv2.TM_CCORR_NORMED')
    result = cv2.matchTemplate(img, template,
                               method)  # Result is a greyscale where whiter spots are areas where it is more likely a match
    result = np.where(result >= threshold)

    result = zip(*result[::-1])

    result.sort(key=lambda x: x[0])  # Sort the list based on the x positions

    i = 0
    while i < len(result) - 1:
        while (i + 1 < len(result) and result[i + 1][0] == result[i][0] and result[i + 1][1] - result[i][
            1] < 8):  # Delete result if they have the same x position are within 8 pixels in terms of y
            del result[i + 1]
        i += 1

    i = 0
    while i < len(result) - 1:
        if i + 1 < len(result) and result[i + 1][0] != result[i][0] and result[i + 1][0] - result[i][
            0] < 10:  # If there are multiple things at one x coordinate, find the group with the most items and remove the rest
            if sum(x[0] == result[i][0] for x in result) < sum(x[0] == result[i + 1][0] for x in result):
                result = [x for x in result if x[0] != result[i][0]]
            else:
                result = [x for x in result if x[0] != result[i + 1][0]]
            i = -1
        i += 1

    return result


stave = []
HEIGHT = 0
WIDTH = 0
stave_distance = 0
