import Tkinter as tk
from Tkinter import *
from tkFileDialog import askopenfilename
import MIDI
import threading
import ImageDetection as im


class Application(tk.Frame):
    def __init__(self, master=None):
        """
        Creates the GUI frame and the widgets that populate it
        """
        tk.Frame.__init__(self, master)
        self.master.title("Score to MIDI Converter")
        self.master.protocol('WM_DELETE_WINDOW', lambda: close_event(self))
        self.master.iconbitmap('DLLs\crotchet.ico')
        self.grid()
        self.master.resizable(0, 0)

        self.lastFileConverted = ""

        self.message = StringVar()
        self.label1 = tk.Label(self, text="Select an image to convert")
        self.convertButton = tk.Button(self, text='Convert',
                                       command=lambda: convert_click(self, self.fileNameBox.get()))
        self.browseButton = tk.Button(self, text='Browse', command=self.askopenfile)
        self.fileNameBox = tk.Entry(self, width=100)
        self.fileNameBox.config(state=DISABLED)
        self.imageLabel = tk.Label(self)
        self.playButton = tk.Button(self, text='Play', command=lambda: play_click(self, self.lastFileConverted))
        self.stopButton = tk.Button(self, text='Stop', command=stop_click)
        self.label2 = tk.Label(self, textvariable=self.message)

        self.columnconfigure(0, pad=10)
        self.columnconfigure(1, pad=10)

        self.label1.grid(row=0, columnspan=2)
        self.browseButton.grid(row=1, columnspan=2)
        self.fileNameBox.grid(row=2, columnspan=2)
        self.imageLabel.grid(row=3, columnspan=2)
        self.convertButton.grid(row=4, columnspan=2)
        self.label2.grid(row=5, columnspan=2)

    def askopenfile(self):
        """
        Opens a file selection dialog box to allow the user to select an image file
        If a file is selected, stops music from playing and clears the message and the Play and Stop buttons from the screen
        """
        global SCORE_IMAGE
        file_name = askopenfilename(filetypes=(("Image files", "*.png *.jpg *.tif"), ("All files", "*")))
        if file_name is not "":
            self.fileNameBox.config(state=NORMAL)

            self.fileNameBox.delete(0, END)
            self.fileNameBox.insert(0, file_name)
            self.fileNameBox.config(state=DISABLED)

            MIDI.stop_music()
            self.playButton.grid_forget()
            self.stopButton.grid_forget()
            self.message.set("")
            SCORE_IMAGE = im.load_image_for_GUI(file_name)
            if SCORE_IMAGE:
                self.imageLabel.config(image=SCORE_IMAGE)
            else:
                self.message.set("FAILED: File is not an acceptable image")


def close_event(self):
    """
    Called when the the window is closed
    Stops the music from playing before closing the window
    """
    MIDI.stop_music()  # Stop playing music before exiting
    self.master.destroy()


def convert_click(self, file_name):
    """
    Called when the convert button is clicked
    Disables all buttons and input
    Calls the functions to convert the image
    If conversion is successful, add Play and Stop button to GUI
    Re enables all buttons and input

    Parameters:
        file_name - the path of the file to convert
    """

    def callback():
        self.lastFileConverted = file_name
        if im.convert(file_name, self.message):
            self.playButton.grid(row=7, column=0, sticky=E)
            self.stopButton.grid(row=7, column=1, sticky=W)
        self.browseButton.config(state=NORMAL)
        self.convertButton.config(state=NORMAL)

    self.convertButton.config(state=DISABLED)
    self.browseButton.config(state=DISABLED)
    self.fileNameBox.config(state=DISABLED)
    MIDI.stop_music()
    self.playButton.grid_forget()
    self.stopButton.grid_forget()
    self.message.set("Converting...")

    threading.Thread(target=callback).start()


def play_click(self, file_name):
    """
    Takes file name of image converted
    Changes extension to ".mid"
    Start playing the MIDI file
    Parameters:
        file_name - the image that was converted
    """

    def callback():
        midi_file = file_name[:file_name.rfind('.')] + ".mid"  # Cut the extension off the filename and add .mid
        try:
            MIDI.play_music(midi_file)
        except:
            self.message.set("ERROR: File cannot be played")

    threading.Thread(target=callback).start()


def stop_click():
    """
    Stops music if it was playing
    """

    def callback():
        MIDI.stop_music()

    threading.Thread(target=callback).start()


global SCORE_IMAGE
